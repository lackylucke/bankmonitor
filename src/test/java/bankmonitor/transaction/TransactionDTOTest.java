package bankmonitor.transaction;

import bankmonitor.transaction.TransactionDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TransactionDTOTest {

    @Test
    void test_getData() {
        TransactionDTO tr = new TransactionDTO("{ \"reference\": \"foo\", " +
                "\"amount\": 100}");
        assertEquals(tr.getReference(), "foo");
        assertEquals(tr.getAmount(), 100);
    }

    @Test
    void test_getDataMustSetAmountToNegativeOneWhenMissingFromData() {
        TransactionDTO tr = new TransactionDTO("{ \"reference\": \"foo\"}");
        assertEquals(tr.getReference(), "foo");
        assertEquals(tr.getAmount(), -1);
    }

    @Test
    void test_getDataMustSetReferenceToEmptyStringWhenMissingFromData() {
        TransactionDTO tr = new TransactionDTO("{\"amount\": 100}");
        assertEquals(tr.getReference(), "");
        assertEquals(tr.getAmount(), 100);
    }

}