package bankmonitor.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionDTOService {

    private final TransactionService transactionService;

    public List<TransactionDTO> findAll() {
        List<Transaction> entities = transactionService.findAll();
        List<TransactionDTO> transactionDTOS = new ArrayList<>();
        for (Transaction entity : entities) {
            String data = entity.getData();
            TransactionDTO transactionDTO = new TransactionDTO(data);
            transactionDTOS.add(transactionDTO);
        }
        return transactionDTOS;
    }

    public TransactionDTO save(final String data) {
        Transaction transaction = new Transaction(data);
        Transaction response = transactionService.save(transaction);
        return new TransactionDTO(response.getData());
    }

    public ResponseEntity<TransactionDTO> update(final Long id,
                                                 final String data) {
        try {
            Transaction transaction = new Transaction(data);
            Transaction updated = transactionService.update(id, transaction);
            TransactionDTO response = new TransactionDTO(updated.getData());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (TransactionNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


}
