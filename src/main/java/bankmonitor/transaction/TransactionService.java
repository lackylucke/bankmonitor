package bankmonitor.transaction;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static bankmonitor.transaction.TransactionDTO.AMOUNT_KEY;
import static bankmonitor.transaction.TransactionDTO.REFERENCE_KEY;

@Service
@RequiredArgsConstructor
public class TransactionService {

    public static final String ID_NOT_FOUND = "ID not found! ID: ";

    private final TransactionRepository repository;

    public List<Transaction> findAll() {
        return repository.findAll();
    }

    public Transaction findById(final Long id)
            throws TransactionNotFoundException {
        Optional<Transaction> optionalTransaction = repository.findById(id);
        if (optionalTransaction.isEmpty()) {
            throw new TransactionNotFoundException(ID_NOT_FOUND + id);
        }
        return optionalTransaction.get();
    }

    public Transaction save(final Transaction entity) {
        return repository.save(entity);
    }

    public Transaction update(final Long id, final Transaction update)
            throws TransactionNotFoundException {
        try {
            Transaction entity = findById(id);
            JSONObject dbJson = new JSONObject(entity.getData());
            JSONObject updateJson = new JSONObject(update.getData());
            updateAmount(dbJson, updateJson);
            updateReference(dbJson, updateJson);
            entity.setData(dbJson.toString());
            return repository.save(entity);
        } catch (TransactionNotFoundException e) {
            throw new TransactionNotFoundException(ID_NOT_FOUND + id);
        }
    }

    private void updateAmount(final JSONObject dbJson,
                              final JSONObject updateJson) {
        if (updateJson.has(AMOUNT_KEY)) {
            dbJson.put(AMOUNT_KEY, updateJson.getInt(AMOUNT_KEY));
        }
    }

    private void updateReference(final JSONObject dbJson,
                                 final JSONObject updateJson) {
        if (updateJson.has(REFERENCE_KEY)) {
            dbJson.put(REFERENCE_KEY, updateJson.getString(REFERENCE_KEY));
        }
    }


}
