package bankmonitor.transaction;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;


@RequiredArgsConstructor
public class TransactionDTO {

    public static final String REFERENCE_KEY = "reference";
    public static final String AMOUNT_KEY = "amount";
    public static final int _1 = -1;
    public static final String EMPTY_STRING = "";


    @Getter
    private String data;

    public TransactionDTO(final String data) {
        this.data = data;
    }

    public Integer getAmount() {
        Integer returnValue = _1;
        JSONObject jsonData = new JSONObject(this.data);
        if (jsonData.has(AMOUNT_KEY)) {
            returnValue = jsonData.getInt(AMOUNT_KEY);
        }
        return returnValue;
    }

    public String getReference() {
        String returnValue = EMPTY_STRING;
        JSONObject jsonData = new JSONObject(this.data);
        if (jsonData.has(REFERENCE_KEY)) {
            returnValue = jsonData.getString(REFERENCE_KEY);
        }
        return returnValue;
    }

}