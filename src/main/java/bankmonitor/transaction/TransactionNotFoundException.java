package bankmonitor.transaction;

public class TransactionNotFoundException extends Exception {

    public TransactionNotFoundException(final String message) {
        super(message);
    }

}
