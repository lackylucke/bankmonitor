package bankmonitor.transaction;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionDTOService transactionDTOService;

    @ResponseBody
    @GetMapping("/transactions")
    public List<TransactionDTO> findAll() {
        return transactionDTOService.findAll();
    }

    @ResponseBody
    @PostMapping("/transactions")
    public TransactionDTO create(@RequestBody String data) {
        return transactionDTOService.save(data);
    }

    @ResponseBody
    @PutMapping("/transactions/{id}")
    public ResponseEntity<TransactionDTO> update(@PathVariable Long id,
                                                 @RequestBody String data) {
        return transactionDTOService.update(id, data);
    }

}