package bankmonitor.transaction;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "transaction")
public class Transaction {

    public static final int MAX_DATA_LENGTH = 1000;
    public static final String DATA_NOT_BLANK_MSG = "Data is mandatory!";
    public static final String DATA_MAX_LENGTH_MSG = "Max data length is ";
    public static final String DATA_COLUMN_NAME = "data";
    public static final String CREATED_AT_COLUMN_NAME = "created_at";
    public static final String NOT_NULL_MSG = "createdAt field is mandatory!";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = DATA_COLUMN_NAME)
    @NotBlank(message = DATA_NOT_BLANK_MSG + MAX_DATA_LENGTH)
    @Length(max = MAX_DATA_LENGTH, message = DATA_MAX_LENGTH_MSG)
    private String data;

    @Column(name = CREATED_AT_COLUMN_NAME)
    @NotNull(message = NOT_NULL_MSG)
    private LocalDateTime createdAt;

    public Transaction(final String data) {
        this.data = data;
        this.createdAt = LocalDateTime.now();
    }

}
