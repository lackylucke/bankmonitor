package bankmonitor.transaction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TransactionDTOControllerTest {

    public static final String HOST = "http://localhost:";
    public static final String ENDPOINT_TRANSACTIONS = "/transactions";
    public static final String UPDATE_ENDPOINT_1 = "/transactions/1";
    public static final String UPDATE_ENDPOINT_2 = "/transactions/2";
    public static final String UPDATE_ENDPOINT_3 = "/transactions/3";
    public static final String UPDATE_ENDPOINT_4 = "/transactions/4";
    public static final String NON_EXISTING_UPDATE_ENDPOINT
            = "/transactions/100";
    public static final int EXPECTED_LIST_SIZE = 5;
    public static final String BAD_REQUEST = "400 BAD_REQUEST";
    public static final String EMPTY_STRING = "";

    @Autowired
    private TransactionService transactionService;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void getAllTransactionsMustReturnAllTransaction()
            throws URISyntaxException {
        List<TransactionDTO> transactionDTOS = findAllTransactions();
        Assertions.assertEquals(EXPECTED_LIST_SIZE, transactionDTOS.size());
    }

    @Test
    void createTransactionMustInsertTransactionWithTheGivenDataToDatabase()
            throws URISyntaxException {
        URI uri = new URI(HOST + port + ENDPOINT_TRANSACTIONS);
        String data = "{ \"amount\": 11111, \"reference\": \"TEST\" }";
        HttpEntity<String> request = new HttpEntity<>(data);
        ResponseEntity<TransactionDTO> response
                = restTemplate.postForEntity(uri, request,
                TransactionDTO.class);
        List<TransactionDTO> transactionDTOS = findAllTransactions();
        TransactionDTO savedTransactionDTO = null;
        for (TransactionDTO transactionDTO : transactionDTOS) {
            if (data.equals(transactionDTO.getData())) {
                savedTransactionDTO = transactionDTO;
            }
        }
        Assertions.assertEquals(
                savedTransactionDTO.getData(),
                response.getBody().getData());
    }

    @Test
    void updateTransactionMustUpdateTheAmountAndValueDataInDatabase()
            throws URISyntaxException, TransactionNotFoundException {
        URI uri = new URI(HOST + port + UPDATE_ENDPOINT_1);
        String data = "{ \"amount\": 11111, \"reference\": \"TEST\" }";
        HttpEntity<String> request = new HttpEntity<>(data);
        restTemplate.put(uri, request);
        Transaction transaction = transactionService.findById(1L);
        TransactionDTO dto = new TransactionDTO(transaction.getData());
        assertEquals("TEST", dto.getReference());
        assertEquals(11111, dto.getAmount());
    }

    @Test
    void updateTransactionMustUpdateTheAmountDataInDatabase()
            throws URISyntaxException, TransactionNotFoundException {
        URI uri = new URI(HOST + port + UPDATE_ENDPOINT_2);
        String data = "{ \"amount\": 11111}";
        HttpEntity<String> request = new HttpEntity<>(data);
        restTemplate.put(uri, request);
        Transaction transaction = transactionService.findById(2L);
        TransactionDTO dto = new TransactionDTO(transaction.getData());
        assertEquals(EMPTY_STRING, dto.getReference());
        assertEquals(11111, dto.getAmount());
    }

    @Test
    void updateTransactionMustUpdateTheReferenceDataInDatabase()
            throws URISyntaxException, TransactionNotFoundException {
        URI uri = new URI(HOST + port + UPDATE_ENDPOINT_3);
        String data = "{ \"reference\": \"TEST\" }";
        HttpEntity<String> request = new HttpEntity<>(data);
        restTemplate.put(uri, request);
        Transaction transaction = transactionService.findById(3L);
        TransactionDTO dto = new TransactionDTO(transaction.getData());
        assertEquals("TEST", dto.getReference());
        assertEquals(-100, dto.getAmount());
    }

    @Test
    void updateTransactionWithNonExistedIdMustThrownException()
            throws URISyntaxException {
        URI uri = new URI(HOST + port + NON_EXISTING_UPDATE_ENDPOINT);
        String data = "{ \"reference\": \"TEST\" }";
        HttpEntity<String> request = new HttpEntity<>(data);
        ResponseEntity<TransactionDTO> transaction = restTemplate.exchange(
                uri,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<TransactionDTO>() {
                }
        );
        assertEquals(BAD_REQUEST, transaction.getStatusCode().toString());
    }

    @Test
    void updateTransactionWithBlankDataMustThrownException()
            throws URISyntaxException {
        URI uri = new URI(HOST + port + UPDATE_ENDPOINT_4);
        HttpEntity<String> request = new HttpEntity<>(EMPTY_STRING);
        ResponseEntity<TransactionDTO> transaction = restTemplate.exchange(
                uri,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<TransactionDTO>() {
                }
        );
        assertEquals(BAD_REQUEST, transaction.getStatusCode().toString());
    }

    @Test
    void updateTransactionWithNullDataMustThrownException()
            throws URISyntaxException {
        URI uri = new URI(HOST + port + UPDATE_ENDPOINT_4);
        HttpEntity<String> request = new HttpEntity<>(null);
        ResponseEntity<TransactionDTO> transaction = restTemplate.exchange(
                uri,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<TransactionDTO>() {
                }
        );
        assertEquals(BAD_REQUEST, transaction.getStatusCode().toString());
    }

    private List<TransactionDTO> findAllTransactions() throws URISyntaxException {
        URI uri = new URI(HOST + port + ENDPOINT_TRANSACTIONS);
        ResponseEntity<List<TransactionDTO>> response = restTemplate.exchange(
                uri,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TransactionDTO>>() {
                }
        );
        return response.getBody();
    }

}