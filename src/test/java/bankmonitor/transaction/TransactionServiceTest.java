package bankmonitor.transaction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TransactionServiceTest {

    public static final int EXPECTED_LIST_SIZE = 5;
    public static final String ID_NOT_FOUND_MSG = "ID not found! ID: ";

    @Autowired
    private TransactionService transactionService;

    @Test
    void findAllMustReturnAllTransactionsFromDatabase() {
        List<Transaction> transactions = transactionService.findAll();
        Assertions.assertEquals(EXPECTED_LIST_SIZE, transactions.size());
    }

    @Test
    void findByIdMustThrowExceptionWhenNonExistedIdGiven() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            Transaction transaction = transactionService.findById(111L);
        });
        Assertions.assertEquals(ID_NOT_FOUND_MSG + 111L, thrown.getMessage());
    }

    @Test
    void saveMustCreateCreatedAtValue() {
        String data = "{ \"reference\": \"TEST\" }";
        Transaction transaction = new Transaction(data);
        Transaction saved = transactionService.save(transaction);
        Assertions.assertNotNull(saved.getCreatedAt());
    }

    @Test
    void saveMustCreateCreateId() {
        String data = "{ \"reference\": \"TEST\" }";
        Transaction transaction = new Transaction(data);
        Transaction saved = transactionService.save(transaction);
        Assertions.assertNotNull(saved.getId());
    }

}